const Webpack = require('webpack')
const path = require('path')
const autoprefixer = require('autoprefixer')          // https://www.npmjs.com/package/autoprefixer

const IS_DEV = process.env.NODE_ENV === 'development' // https://nodejs.org/api/process.html#process_process , https://hub.packtpub.com/building-better-bundles-why-processenvnodeenv-matters-optimized-builds/
const plugins = [                                     // https://webpack.js.org/concepts/plugins/
  new Webpack.LoaderOptionsPlugin({                   // https://v4.webpack.js.org/plugins/loader-options-plugin/
    options: {
      postcss: [ autoprefixer ]
    }
  })
]

if (IS_DEV) {
  plugins.push(
  new Webpack.HotModuleReplacementPlugin(),           // https://github.com/webpack/docs/wiki/hot-module-replacement , //https://webpack.js.org/plugins/hot-module-replacement-plugin/
    new Webpack.NamedModulesPlugin()                  // https://webpack-v3.jsx.app/plugins/named-modules-plugin/
  )
}

// https://webpack.js.org/concepts/

module.exports = {                                    // https://webpack.js.org/configuration/module/
  mode: process.env.NODE_ENV,
  devtool: 'inline-source-map',
  entry: {
    // 'index': './src/index',
    'Tree': './src/index'
  },

  // https://alligator.io/nodejs/how-to-use__dirname/ (about when to use __dirname)
  // https://nodejs.org/api/path.html#path_path_join_paths (about nodejs path module)
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',                            // https://webpack.js.org/configuration/output/#outputfilename
    library: '[name]',
    libraryTarget: 'umd'                             // https://webpack.js.org/guides/author-libraries/
  },
  plugins,
  resolve: {
    modules: [
      'src',
      'node_modules'
    ],
    extensions: ['.js', '.jsx']
  },

  // https://webpack.js.org/configuration/module/
  module: {
    rules: [
      {
        test: /\.less$/,
        loaders: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'less-loader'
        ]
      }, {
        test: /\.(js|jsx)$/,
        include: [
          path.join(__dirname, 'src'),
          path.resolve(__dirname, 'node_modules/sc-pattern-library/')
        ],
        loaders: ['babel-loader']
      }, {
        test: /bootstrap\/js\//,
        loader: 'imports?jQuery=jquery'
      }
    ]
  },
  // externals: {
  //     'react': 'react',
  //     'react-dom': 'react-dom',
  //     'prop-types': 'prop-types',
  //     'sc-pattern-library': 'patternLibrary',
  //     'i18next': 'i18next',
  //     'lodash': '_',
  //     'react-markdown': 'react-markdown'
  //   }
}
