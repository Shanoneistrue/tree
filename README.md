# README #

This is a Temaplate of general  React App with nodeJs Environment and Webpack as module bundler.

You can clone it using the repo url from bitbucket with ssh/http.

To run this project in your local you should have node installed in your machine.

After the cloning part is done run the `npm init` command to initialize node. Next step is to do an `npm i or npm install` to install the dependencies.

Once the initialization of node and installation of dependencies is done run `npm start` script to start the project. After all the code is compiled successfully got to `localhost:8080` in the browser to see the project output.

# Why do we need @babel/core.
Because @babel/core is the dependecy package for most of other babel packages.
if you tried to uninstall @babel/core then npm yells at you with warnings similar to
`@babel/plugin-transform-template-literals@7.8.3 requires a peer of @babel/core^7.0.0-0 but none is installed. You must install peer dependencies yourself.`

# Why do we need @babel/plugin-proposal-class-properties
`https://medium.com/@jacobworrel/babels-transform-class-properties-plugin-how-it-works-and-what-it-means-for-your-react-apps-6983539ffc22`

# Why do we need @babel/preset-env
`https://babeljs.io/docs/en/babel-preset-env#docsNav`

# Why do we need @bable-loader
`The configuration is quite minimal. For every file with a js or jsx extension Webpack pipes the code through babel-loader. With this in place we're ready to write a React.`

source: `https://www.valentinog.com/blog/babel/`

I think similar to the above process webpack pipes all less files through `less-loader` and css files though `css-loader` and the same way for other file extensions.


# How Webpack-loader plugins work.
A loader is just a file that exports a function. The compiler calls this function and passes the result of the previous loader or the resource file into it. The this context of the function is filled-in by the compiler with some useful methods that allow the loader to, among other things, change its invocation style to async or get query parameters. The first loader is passed one argument: the content of the resource file. The compiler expects a result from the last loader. The result should be a String or a Buffer (which is converted to a string), representing the JavaScript source code of the module. An optional SourceMap result (as JSON object) may also be passed.
source: `https://github.com/webpack/docs/wiki/loaders`
